package com.digisafari.sapl.courseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseServiceApplication {

	public static void main(String[] args) {
		System.out.println("application is boosted..");
		SpringApplication.run(CourseServiceApplication.class, args);
	}

}
